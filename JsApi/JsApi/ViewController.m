//
//  CNViewController.m
//  ModuleApi
//
//  Created by Rajitha Egodaarachchi on 10/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "JsApi.h"

@interface ViewController ()

@property (nonatomic, strong) NSURL *fileURL;

@end

@implementation ViewController
{
    JsApi * _jsApi;
    NSURLRequest *_urlRequest;
    NSURLRequest *_urlRequestToLoad;
}

@synthesize fileURL = _fileURL;
@synthesize FileName = _FileName;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _jsApi = [[JsApi alloc] init];
	_jsApi.delegate = self;
    
    [self initWebView];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (self.FileName == nil)
    {
        self.FileName = @"index.html";
    }
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:self.FileName ofType:nil];
	NSString *strWebViewRequestUrl = [_webView.request.URL relativeString];				
	NSRange filePathRange = [strWebViewRequestUrl rangeOfString:filePath];
	
	if (filePathRange.length == 0) {
		[_webView removeFromSuperview];		
		
		if (filePath && [[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
			self.fileURL = [NSURL fileURLWithPath:filePath];
			
			_urlRequest = [NSURLRequest requestWithURL:self.fileURL 
                                           cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                       timeoutInterval:5]; 
			
			if (_webView.isLoading) {
				_urlRequestToLoad = _urlRequest;
			}
			else {
				_webView = nil;
				[self initWebView];
				
				[_webView loadRequest:_urlRequest];
			}
		}
		else {
			_urlRequest = nil;
		}
	}
}

- (NSURL *)getDocumentDirectoryURL
{
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)initWebView
{
	if (_webView == nil) {
		_webView = [[UIWebView alloc] init];
	}
	
	_webView.frame = self.view.frame;
	_webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
	_webView.delegate = self;
	_webView.scalesPageToFit = YES;	
}

- (void)viewDidUnload
{
    _webView = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (BOOL)viewAsset:(NSString *)asset
{	
	NSString *filePath = [[NSBundle mainBundle] pathForResource:asset ofType:nil];
	
    if (filePath != nil) {
		ViewController *viewController = [[ViewController alloc] init];
        viewController.FileName = asset;
		self.view = viewController.view;	
		return YES;
	}
	
	return NO;
}

# pragma mark -
# pragma CNModuleApiDelegate methods

-(NSString *) excecuteScript:(NSString *)script
{
	return [_webView stringByEvaluatingJavaScriptFromString:script];
}

# pragma mark -
#pragma UIWebViewDelegate methods

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	if([_jsApi isApiRequest:request]) {
		[_jsApi dispatch:request];
		return NO;
	}
	
	return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	if (![self.view.subviews containsObject:_webView]) {
		[self.view addSubview:_webView];
	}
	
	if (_urlRequestToLoad) {
		if (![webView.request.URL.lastPathComponent isEqualToString:_urlRequestToLoad.URL.lastPathComponent]) {
			[webView loadRequest:_urlRequestToLoad];
		}
		else {
			_urlRequestToLoad = nil;
		}
	}
}

@end
