//
//  JsApiDelegate.h
//  Macs
//
//  Created by Chamindu R. Munasinghe on 6/11/12.
//  Copyright (c) 2012 CompareNetworks, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JsApiDelegate <NSObject>

- (NSString *) excecuteScript: (NSString *) script;

- (BOOL)viewAsset:(NSString *)asset;

@end
