//
//  CNViewController.h
//  ModuleApi
//
//  Created by Rajitha Egodaarachchi on 10/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JsApiDelegate.h"

@interface ViewController : UIViewController <JsApiDelegate, UIWebViewDelegate>
{
    __strong IBOutlet UIWebView *_webView;
}

@property (nonatomic, strong) NSString *FileName;

@end
