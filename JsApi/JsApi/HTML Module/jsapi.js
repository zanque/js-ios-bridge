(function(w, d) {
 var api = {
	initApi : function(success, failure) {
		dispatch("initApi", {}, success, failure);
	},
 
    viewAsset : function(id, success, failure) {
		dispatch("viewAsset", {assetId : id}, success, failure);
	}, 
 
    getCommand : function(key) {
        var cmd = _commands[key];
        return JSON.stringify(cmd);
    },
    
    callback : function(key, success, result) {
        var cmd = _commands[key];
        if(success) {
            cmd.success(result);
        }
        else {
            cmd.failure(result);
        }
        delete _command[key];
    }
 };
 
 var _commands = {};
 var _commandId = 0;
 
 function dispatch(command, arguments, successCallback, faliureCallback) {
    var key = "n" + (++ _commandId); 
    var commandObject = {cmd : command, args : arguments, success : successCallback, failure : faliureCallback};
    _commands[key] = commandObject;
    notify(key);
 }
 
 function notify(key) {
    var iframe = d.createElement("IFRAME");
    iframe.setAttribute("src", "jsapi://" + key);
    d.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
 }
 
 w.jsapi = api;
 
})(window, document);