//
//  JsApi.h
//  Macs
//
//  Created by Chamindu R. Munasinghe on 6/11/12.
//  Copyright (c) 2012 CompareNetworks, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "JsApiDelegate.h"

@interface JsApi : NSObject

@property (nonatomic, weak) id <JsApiDelegate> delegate;

- (BOOL) isApiRequest: (NSURLRequest *) request;
- (NSString *) dispatch: (NSURLRequest *) request;

@end
